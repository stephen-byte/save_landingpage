
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/form.css">
    <meta charset="UTF-8">
    <title>Join us</title>
</head>
<body>

<form action="form.php" method="POST">
  <div  id="askType">
    <header class="showcase">
      <h1>Thank you for Joining Us !</h1>
    </header>
    <div  class=" content1 container">
      Would you please tell us which one are you?
    </div>
    <div id="inputs">
      <input type="checkbox" name="registeredType[]" value="Company" ><label>Company</label></Br>
      <input type="checkbox" name="registeredType[]" value="Individual" ><label>Individual</label>
      <input type="button" onclick="displayNextQuestion('askName','block', 'askType', 'none' );" value="Next">
    </div>
  </div>
  <div id="askName">
    <header class="showcase">
      <h1>Thank you for Joining Us !</h1>
    </header>
    <div class="content2 container">
      Would you please tell your name?
    </div>
    <div id="name">
      <input type="text" name="myName" placeholder="Name" >
      <input type="button"  onclick="displayNextQuestion('askMailAdress','block', 'askName', 'none')" value="Next">
    </div>
  </div>
  <div id="askMailAdress">
    <header class="showcase">
      <h1>Let us send you our last activities</h1>
    </header>
    <div class="content1 container">
      Do you have a mail adress to give us?
    </div>
    <div id="mail">
      <input type="mail" name="mailAdress" placeholder="contact@savetechnologies.com" >
      <input type="button"  onclick="displayNextQuestion('askPhoneNumber','block', 'askMailAdress', 'none')" value="Next">
    </div>
  </div>
  <div id="askPhoneNumber">
    <header class="showcase">
      <h1>We want to be closer with you</h1>
    </header>
    <div  class="content2 container">
      Would it be possible to have a phone number?
    </div>
    <div id="phoneNumber">
      <input type="mail" name="phoneNumber" >
      <input type="button"  onclick="displayNextQuestion('askActivities','block', 'askPhoneNumber', 'none')" value="Next">
    </div>
  </div>
  <div id="askActivities">
    <header class="showcase">
      <h1>One last question </h1>
    </header>
    <div class="content1 container">
      What are you going to use our product for?
    </div>
    <div id="activicty">
      <input type="checkbox" name="activity[]" value="Cars" ><label>Cars</label></Br>
      <input type="checkbox" name="activity[]" value="Real Estate" ><label>Real Estate</label></Br>
      <input type="checkbox" name="activity[]" value="Resume" ><label>Resume</label></Br>
      <button type="submit"> Submit my answers </button>
    </div>
  </div>
</form>

<div id="thankYou">
  <header class="showcase">
    <h1>Thank you so much for joining us !</h1>
  </header>
  <div class="content2" >
    We will comeback to you shortly?
  </div>
</div>
<script src="js/form.js"></script>
</body>
</html>
